package com.example.driverlicenceocr

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class DriverLicence(private val raw: String) {
    var firstname: String = ""
    var lastname: String = ""
    var dob: String = ""
    var gender: String = ""
    var licenseNumber: String = ""

    init {
        if (raw.contains("/")) {
            /** card 1 **/
            if (raw.matches(Regex(".*/.*\\w{0,2}[C,c][E,e][N,n][C,c][E,e](\\s[N,n][O,o])?/.*/[D,d][O,o][B,b,S,s]\\s?[0-9]{2}\\s?[a-zA-Z]{3}\\s?[0-9]{4}([\\s,/]?)([S,s][E,e][X,x]\\s?[a-zA-Z]{1})?/.*"))){
                var result = raw.replace(Regex("/[A,a][U,u][S,s]([a-zA-Z]{0,7}?)/[D,d][O,o][B,b,S,s]"),"/DOB")
                result = result.compileRegex("[D,d][R,r][I,i][V,v][E,e][R,r] [L,i][I,i][C,c][E,e][N,n][C,c][E,e]/.*/[D,d][O,o][B,b,S,s]")
                result = result.replace(Regex("[D,d][R,r][I,i][V,v][E,e][R,r] [L,i][I,i][C,c][E,e][N,n][C,c][E,e]/"),"").replace(Regex("/[D,d][O,o][B,b,S,s]"),"").replace(Regex("\\w{0,2}[C,c][E,e][N,n][C,c][E,e](\\s[N,n][O,o])?/"), "")
                val result_list = result.split("/").toMutableList()
                lastname = result_list.first { it.matches(Regex("^[A-Z]*$")) }
                result_list.remove(lastname)
                lastname = lastname.properCase()
                result_list.forEach {
                    if (it.matches(Regex("^[A-Z,\\s]*$"))){
                        firstname += it.properCase()
                    }
                    if (it.matches(Regex("^[0-9,\\s]*$"))){
                        licenseNumber = it.removeAllSpace()
                    }
                }
                dob = compileRegex("[D,d][O,o][B,b,S,s]\\s?[0-9]{2}\\s?[a-zA-Z]{3}\\s?[0-9]{4}").toLowerCase().removePrefix("dob").removePrefix("dos").removeAllSpace().formatDate("ddMMMyyyy")
                gender = compileRegex("[S,s][E,e][X,x]\\s?[a-zA-Z]{1}").toUpperCase().removePrefix("SEX").trim()
            }
            if (raw.matches(Regex(".*/?(\\w{2}?)CENCE NO/.*/"))) {
                licenseNumber = compileRegex("(\\w{2}?)CENCE NO/.*/")
                    .split("/")[1].removeAllSpace()
            }
            /** card 2 **/
            if (raw.matches(Regex(".*/1\\..*/2\\..*/3\\. LICENCE NO.*/.*"))) {
                val result = compileRegex("1\\..*/").split("/")
                firstname = result[0].removePrefix("1.").trim().properCase()
                lastname = result[1].removePrefix("2.").trim().properCase()
                val result2 = result[2].removePrefix("3.").trim()
                if (result2.startsWith("LICENCE NO ")) {
                    licenseNumber =
                        result2.removePrefix("LICENCE NO ").substring(0, result2.indexOf(" "))
                }

                if (raw.matches(Regex(".*EXPIRY DATE/([0-9]{2})/([0-9]{2})/([0-9]{4})/([0-9]{2})/([0-9]{2})/([0-9]{4})/"))) {
                    dob =
                        compileRegex("EXPIRY DATE/([0-9]{2})/([0-9]{2})/([0-9]{4})/").removePrefix("EXPIRY DATE/")
                            .replace("/", " ").trim()
                    dob = dob.formatDate("dd MM yyyy")
                }
            }
            /** card 3 **/
            if (raw.matches(Regex(".*New South Wales, Australia/.*/Card Number/.*"))) {
                val result = compileRegex("New South Wales, Australia/.*/Card Number/.*").split("/")
                val fullname = result[1].split(" ").toMutableList()
                lastname = fullname.removeFirst().properCase()
                firstname = fullname.joinToString(" ").properCase()
                licenseNumber = result[3].removeAllSpace()

                if (raw.matches(Regex(".*/([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})/.*/Expiry Date/([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})/"))) {
                    val result =
                        compileRegex("([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})/.*/Expiry Date/([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})")
                    dob = result.split("/")[0]
                    dob = dob.formatDate("dd MMM yyyy")
                }
            }
            /** card 4 **/
            if (raw.matches(Regex(".*/.*[Ll][Ii][Cc][.]?[NHn][Oo].*/.*"))) {
                val result = raw.split("/")
                val index = result.indexOf(result.find { it.contains("EXPIRE",ignoreCase = true) })
                lastname = result[index+1].properCase()
                firstname = result[index+2].properCase()
                licenseNumber = compileRegex("[0-9]{6,}")
                dob = compileRegex("([0-9]{1,})[\\s]?([a-zA-Z]{3})[\\s]?([0-9]{4})").removeAllSpace().formatDate("ddMMMyyyy")
            }
            /** card 5 **/
            if (raw.matches(Regex(".*[Cc][Oo][Nn][Dd][Ii][Tt][Ii][Oo][Nn][Ss]/.*([0-9]{2})/([0-9]{2})/([0-9]{4}).*"))) {
                val result = compileRegex("[Cc][Oo][Nn][Dd][Ii][Tt][Ii][Oo][Nn][Ss]/.*")
                licenseNumber = result.compileRegex("[/\\s]+[a-zA-Z0-9]{6}[/\\s]+").replace("/", "")
                dob = result.compileRegex("([0-9]{2})/([0-9]{2})/([0-9]{4})")
                val fullname =
                    result.compileRegex("[A-Za-z]{2,} [A-Za-z]{2,}( [A-Za-z]{2,})*( [A-Za-z]{2,})*").split(" ")
                        .toMutableList()
                lastname = fullname.removeLast().properCase()
                firstname = fullname.joinToString(" ").properCase()
            }
            /** card 6 **/
            if (raw.matches(Regex(".*[Aa][Uu][Ss][Tt][Rr][Aa][Ll][Ii][Aa][Nn] [Cc][Aa][Pp][Ii][Tt][Aa][Ll] [Tt][Ee][Rr][Rr][Ii][Tt][Oo][Rr][Yy]([\\s/]{1}).*"))) {
                val result = compileRegex("[Aa][Uu][Ss][Tt][Rr][Aa][Ll][Ii][Aa][Nn] [Cc][Aa][Pp][Ii][Tt][Aa][Ll] [Tt][Ee][Rr][Rr][Ii][Tt][Oo][Rr][Yy]([\\s/]{1}).*").split("/")
                val result2 = result.subList(1, result.size).joinToString("/")
                val fullname =
                    result2.compileRegex("[A-Z]{2,} [A-Za-z]{2,}( [A-Za-z]{2,})*( [A-Za-z]{2,})*").split(" ")
                        .toMutableList()
                lastname = fullname.removeFirst().properCase()
                firstname = fullname.joinToString(" ").properCase()
                dob = result2.compileRegex("([0-9]{2})([A-Za-z]{3})([0-9]{4})")
                dob = dob.formatDate("ddMMMyyyy")
                licenseNumber = result2.compileRegex("[Nn][Ooa].?\\s+[a-zA-Z0-9]{10}").split(" ")[1]
            }
            /** card 7 **/
            if (raw.matches(Regex(".*VICTORIA AUSTRALIA/.*/LICENCE NO/[0-9]{5,}/.*"))) {
                val fullname =
                    compileRegex("VICTORIA AUSTRALIA/.*").split("/")[1].split(" ").toMutableList()
                lastname = fullname.removeFirst().properCase()
                firstname = fullname.joinToString(" ").properCase()
                dob = compileRegex("DATE OF BIRTH/([0-9]{2})-([0-9]{2})-([0-9]{4})").split("/")[1]
                dob = dob.formatDate("dd-MM-yyyy")
            }
        }
    }

    private fun compileRegex(pattern: String): String {
        val pattern = Pattern.compile(pattern)
        val matcher = pattern.matcher(raw)
        while (matcher.find()) {
            return matcher.group()
        }
        return ""
    }

    fun String.compileRegex(pattern: String): String {
        val pattern = Pattern.compile(pattern)
        val matcher = pattern.matcher(this)
        while (matcher.find()) {
            return matcher.group()
        }
        return ""
    }

    fun String.formatDate(format: String): String {
        try {
            val originalFormat = SimpleDateFormat(format)
            val requiredFormat = SimpleDateFormat("dd/MM/yyyy")
            return requiredFormat.format(originalFormat.parse(this))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return this
    }

    private fun String.properCase() = this.lowercase()
        .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

    private fun String.removeAllSpace() = this.replace(" ", "")

    override fun toString(): String =
        "Lastname: $lastname\nFirstname: $firstname\nDate of birth: $dob\nLicence Number: $licenseNumber\nGender: $gender"

    /** LOGGING FUNCTION FOR TESTING **/
    fun echo(log:String) = Log.d("testing", log)
}