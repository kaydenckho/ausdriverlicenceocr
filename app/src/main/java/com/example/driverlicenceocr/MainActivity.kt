package com.example.driverlicenceocr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val a = "LICENCE NO/082 835 927/ralla/Driver Licence/SMITH/RUTH EMMA/Aus/DOB 21 Apr 1974 Sex F/Height 168/Class/Effective/Type/C 14.01.15 14.01.20/Expiry/Conditions/Queensland/Government/Queensland, Australia/Drive safely/"
        val b = "NORTHERN TERRITORY OF AUSTRALIA/DRIVER LICENCE/AKU NT OTAOKA/ANTOisocUTNTGTNKTAN CUcSTRUANT oAOKAu/Karors/1. CITIZEN/2. JANE/3. LICENCE NO 1564102 4 CLASS MR/5. 2 SAMPLE ST/ROADSAFETY NT 0800/25/12/1999/6./Signatmo Brip/7. DATE OF BIRTH 8. EXPIRY DATE/25/12/1999/01/06/2021/"
        val c = "Driver Licence/New South Wales, Australia/Sara Baumer/Card Number/2 093 628 291/4 East St/ROSE BAY NSW 2029/Llee non No./132836991/ucece Class/C/Oemor/A/Dat of B/14 MAY 1990/Sara Baumer/Expiry Date/12 JUN 2013/"
        val d = "DRIVER LICENCE/TASMANIA AUSTRALIA/LIC.NO 5526927 asmania/D.o.B/15 NOV 1972/FULL CEXPIRE S 31 MAR 2010/CliizEN/JOSERH/100 TASNANIA ROAD/SNUG 7054/DEPARTMENT OF INFRASTRUCTURE, ENERGY AND RE SOURCES/"
        val e = "DRIVER S LICENCE/SOUTH AUSTRALIA/Licence No/Date Of Birth Expiry Date/Conditions/W65042/CLASS C/10/04/1960/ww/D104/2014/01/TR/JANE CITIZEN/3 THIRD ST/ADELAIDE 5000/ORGAN/DONDR/www./"
        val f = "DRIVER LICENCE/AUSTRALIAN CAPITAL TERRITORY AG/CITIZEN Joan/13 CHALLIS ST/DICKSON ACT 2602/Oute of Bith 30AUG1985erE/Licence No. 1234567890 31DEC2020/Licence ClassC.R/Condtions S/Je/"
        val g = "DRIVER LICENCE/VICTORIA AUSTRALIA/JANE CITIZEN/LICENCE NO/987654321/FLAT 10E5/77 SAMPLE PARADE/KEW EAST VIC 3102/LICENCE EXPIRY/20-05-2019/DATE OF BIRTH/29-07-1983/LICENCE TYPE/CAR/CONDITIONS/SBEAVXYZZ/Qmen/Victo/vicroads/"

        val sampleInstance = DriverLicence(a)
        /** Search "testing" in Logcat below to see result **/
        Log.d("testing",sampleInstance.toString())

    }
}